import time
import win32api
from win32gui import GetForegroundWindow
import win32con
from selenium import webdriver
from selenium.common import exceptions as SX
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from functools import wraps
import inspect


import cfg
import g


def reconnect(url):
    g.d = webdriver.Remote(command_executor=url,desired_capabilities={})
    g.d.session_id = g.session_id

class template_list():
    zone = None
    line = None
    callout = None
    icon = None


def handleError(msg):
    @wraps(msg)
    def _handleError(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            time0 = time.time()
            try:
                return f(*args, **kwargs)
            except SX.StaleElementReferenceException as err:
                print('stale element in ', msg)
                print(err)
                reset_app_state()
                init_static_chart_elements()
            except SX.WebDriverException as err:
                print('web driver exception: ', msg)
                print(err)
                reset_app_state()
            except Exception as err:
                print('unknown exception in ', msg)
                print(err)
                raise err
                reset_app_state()
            print(msg, " Time", time.time() - time0)
        return wrapper
    return _handleError


def errorExit(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except:
            reset_app_state()
            init_static_chart_elements()
            return ''
    return wrapper

def staleRefHandler(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except SX.StaleElementReferenceException:
            init_static_chart_elements()
            return f(*args, **kwargs)
    return wrapper


def print_call_stack():
    curr_frame = inspect.currentframe()
    call_frame = inspect.getouterframes(curr_frame, 0)
    for item in call_frame[1:4]:
        print('      -', item[3])


# class element_has_css_class(object):
#   """An expectation for checking that an element has a particular css class.
#
#   locator - used to find the element
#   returns thev WebElement once it has the particular css class
#   """
#   def __init__(self, locator, css_class):
#     self.locator = locator
#     self.css_class = css_class
#
#   def __call__(self, driver):
#     element = g.d.find_element(*self.locator)   # Finding the referenced element
#     if self.css_class in element.get_attribute("class"):
#         return element
#     else:d
#         return False
#
#
# # Wait until an element with id='myNewInput' has class 'myCSSClass'
# wait = WebDriverWait(driver, 10)
# element = wait.until(element_has_css_class((By.ID, 'myNewInput'), "myCSSClass"))

def send_mouse_click():
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
    time.sleep(0.05)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)


def init_browser():
    # Put this into prefs map to switch off d notification
    prefs = {'profile.default_content_setting_values.notifications': 2}
    options = webdriver.ChromeOptions()
    options.add_argument('--disable-infobars')
    #options.AddExtension(Path.GetFullPath("local/path/to/extension.crx"));
    options.add_experimental_option('prefs', prefs)
    g.d = webdriver.Chrome(chrome_options=options)
    g.builder = ActionChains(g.d)

    g.d.maximize_window()

def is_stale(elem):
    try:
        elem.get_location()
    except SX.StaleElementReferenceException as err:
        return True
    else:
        return False

def get_drawing_tools():
    ignored_exceptions = (SX.NoSuchElementException, SX.StaleElementReferenceException)
    wait = WebDriverWait(g.d, 10, ignored_exceptions=ignored_exceptions)
    elem = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, cfg.drawing_tools_class)))
    return elem

#@errorExit
def init_static_chart_elements():
    g.drawing_tools = get_drawing_tools()
    g.symbol_button = get_element(cfg.symbol_class, By.CLASS_NAME)
    g.alerts_button = get_element(cfg.alerts_button_css)
    # g.magnet_button = get_element(cfg.magnet_css)
    g.magnet_button = get_element(cfg.magnet_class, By.CLASS_NAME)
    g.ruler_button = get_element(cfg.ruler_css)
    g.interval_bar = get_element(cfg.interval_bar_id, By.ID)
    g.window_handle = GetForegroundWindow()
    print('static tools reintialized')


def get_charts_button():
    ignored_exceptions = (SX.NoSuchElementException, SX.StaleElementReferenceException)
    wait = WebDriverWait(g.d, 20, ignored_exceptions=ignored_exceptions)
    #elem = wait.until(EC.element_to_be_clickable((By.XPATH, cfg.charts_button_xp)))
    elem = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, cfg.charts_button_css)))
    # waitfor = get_element(config.last_home_xp, By.XPATH, EC.presence_of_element_located)
    # hover = ActionChains(g.d).move_to_element(charts_button)
    # hover.perform()
    time.sleep(2)
    return elem


# @handleError('get_element()')
def get_element(identifier, ident_type=By.CSS_SELECTOR, wait_type=EC.visibility_of_element_located):

    element = WebDriverWait(g.d, g.ttw).until(wait_type((ident_type, identifier)))
    return element


# @handleError('get_interval')
def get_interval():

    attempts = 0
    while attempts < 5:
        try:
            elem = g.interval_bar.find_element_by_class_name('selected')
            print('    -interval bar read')
            break
        except (SX.StaleElementReferenceException, SX.NoSuchElementException):
            pass

        attempts += 1

    return elem


# @handleError('get_prefix()')
@errorExit
def get_prefix(current_interval):

    return cfg.interval_to_prefix[current_interval.text]


# @handleError('get_postfix()')
def get_postfix(tt_format, prefix):
    return cfg.format_to_postfix[prefix][tt_format]

def click_template_button():
    max_attempts = 6
    # ignored_exceptions = (SX.NoSuchElementException, SX.StaleElementReferenceException)
    # wait = WebDriverWait(g.d, 15, ignored_exceptions=ignored_exceptions)
    # elem = wait.until(EC.vis((By.CSS_SELECTOR, cfg.template_button_css)))
    #
    # return elem
    attempts = 0
    while attempts < max_attempts:
        try:
            wait = WebDriverWait(g.d, 5)
            elem = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, cfg.template_button_css)))
            time.sleep(1)
            elem.click()
            wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,cfg.template_button_dropped_css)))
            print('    -clicked the template button.    Class: ', elem.get_attribute('class'))
            break
        except (SX.StaleElementReferenceException, SX.TimeoutException):
            print('    -looking for the template button')
            pass

        attempts += 1



def get_format_bar():
    wait = WebDriverWait(g.d, 10)
    elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.format_bar_css)))
    return elem

def get_template_popup():
    # ignored_exceptions = (SX.NoSuchElementException, SX.StaleElementReferenceException)
    # wait = WebDriverWait(g.d, 10, ignored_exceptions=ignored_exceptions)
    # elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.template_popup_css)))
    # return elem

    attempts = 0
    while attempts < 5:
        try:
            wait = WebDriverWait(g.d, 10)
            elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.template_popup_css)))
            print('    -found the popup template list.   Class: ', elem.get_attribute('class'))
            break
        except (SX.StaleElementReferenceException, SX.TimeoutException):
            pass

        attempts += 1

    return elem


# @handleError('get_template_names')
def get_template_names(template_list):
    # waitfor = get_element(config.template_popup_css, wait_type=EC.presence_of_all_elements_located)
    attempts = 0
    while attempts < 5:
        try:
            templates = template_list.find_elements_by_class_name('item')
            if g.debug_templates: print('    -number of templates found: ', len(templates), '   Class:', templates[0].get_attribute('class'))
            break
        except SX.StaleElementReferenceException:
            pass

        attempts += 1

    time.sleep(.5)
    return templates


def build_template_list(templates):
    tlist = []

    for t in templates:
        tlist.append(t.text)
        if g.debug_templates: print(t.text)

    return tlist




# @handleError('select_template')
def select_template(template_name):

    # waitfor = get_element(config.template_popup_css, wait_type=EC.presence_of_all_elements_located)
    index = 0
    max_attempts = 6
    popup_list = ''
    templates = ''

    # get the poplist and the elements in the list
    attempts=0
    while attempts < max_attempts:
        attempts += 1

        try:
            popup_list = g.d.find_element_by_css_selector(cfg.template_popup_css)
            if g.debug_zone: print('    -found the popup template list.  Class:', popup_list.get_attribute('class'))
            templates = popup_list.find_elements_by_class_name('item')
        except SX.NoSuchElementException as e:
            if g.debug_zone: print(e)
            click_template_button()
        except SX.StaleElementReferenceException as e:
            if g.debug_zone: print(e)
            popup_list = g.d.find_element_by_css_selector(cfg.template_popup_css)
        else:
            pass

    # build static lists for faster processing if needed and get the index
    if (g.tb.key in cfg.ctrl_line_keys) or (g.tb.key in cfg.alt_line_keys):
        if not g.line_template_list:
            print("   -regenerating line template list ")
            time.sleep(2)
            g.line_template_list = build_template_list(templates)
        index = g.line_template_list.index(template_name)
        if g.debug_zone: print('    -using stored template list')
        g.tb.print_buffer()
    elif (g.tb.key in cfg.ctrl_zone_keys) or (g.tb.key in cfg.alt_zone_keys):
        if not g.zone_template_list:
            print("   -regenerating zone template list ")
            time.sleep(2)
            g.zone_template_list = build_template_list(templates)
        index = g.zone_template_list.index(template_name)
        if g.debug_zone: print('    -using stored zone template list.  Index: ', index)
        g.tb.print_buffer()
    elif (g.tb.key in cfg.ctrl_symbol_keys) or (g.tb.key in cfg.alt_symbol_keys):
        if not g.symbol_template_list:
            print("   -regenerating symbol template list ")
            time.sleep(2)
            g.symbol_template_list = build_template_list(templates)
        index = g.symbol_template_list.index(template_name)
        if g.debug_zone: print('    -using stored symbol template list.  Index: ', index)
        g.tb.print_buffer()
    elif (g.tb.key in cfg.ctrl_text_keys) or (g.tb.key in cfg.alt_text_template_keys):
        if not g.text_template_list:
            print("   -regenerating text template list ")
            time.sleep(2)
            g.text_template_list = build_template_list(templates)
        index = g.text_template_list.index(template_name)
        if g.debug_zone: print('    -using stored text template list.  Index: ', index)
        g.tb.print_buffer()
    elif (g.tb.key in cfg.ctrl_rect_keys) or (g.tb.key in cfg.alt_rect_template_keys):
        if not g.rect_template_list:
            print("   -regenerating rect template list ")
            time.sleep(2)
            g.rect_template_list = build_template_list(templates)
        index = g.rect_template_list.index(template_name)
        if g.debug_zone: print('    -using stored rect template list.  Index: ', index)
        g.tb.print_buffer()
    elif (g.tb.key in cfg.ctrl_ellipse_keys) or (g.tb.key in cfg.alt_ellipse_template_keys):
        if not g.ellipse_template_list:
            time.sleep(2)
            g.ellipse_template_list = build_template_list(templates)
        index = g.ellipse_template_list.index(template_name)
        if g.debug_zone: print('    -using stored ellipse template list.  Index: ', index)
        g.tb.print_buffer()
    else:
        print('   -key not found in select_template(): ', g.tb.key)


    try:
        templates[index].click()
        print('    -clicked the template name')

    except SX.StaleElementReferenceException as e:
        print(e)
    except SX.NoSuchElementException as e:
        print(e)


class element_has_css_class(object):
  """An expectation for checking that an element has a particular css class.

  locator - used to find the element
  returns the WebElement once it has the particular css class
  """
  def __init__(self, locator, css_class):
    self.locator = locator
    self.css_class = css_class

  def __call__(self, driver):
    element = driver.find_element(*self.locator)   # Finding the referenced element
    if self.css_class in element.get_attribute("class"):
        return element
    else:
        return False



def apply_format(tt_format):
    '''type - normal - below
     '''

    template_button = ''
    templates = ''
    template_list = ''


    # get the current selected charts interval
    current_interval = get_interval()

    # add the interval to the tools_bar_keymap buffer
    g.tb.add_interval(current_interval.text)

    # if its the same interval and the same tools_bar_keymap we will not change the last selected format
    # Todo: need to add some exceptions to this
    if g.tb.is_same(interval=current_interval.text):
        return

    # get the template name prefix
    prefix = get_prefix(current_interval)
    if prefix:
        pass
    else:
        return

    # get the template name postfix
    postfix = get_postfix(tt_format, prefix)

    # create the template name
    template_name = prefix + postfix
    print('    -template name: ', template_name)
    # click the template button
    click_template_button()

    # select the correct template
    select_template(template_name)


def reset_app_state():
    print('Resetting the application state')
    if g.debug_reset_app: print('    -{:>} {!r:<}    {:>} {!r:<}    {:>} {!r:<}    {:>} {!r:<}'.
                                format('mouse_key_combo:', g.mouse_key_combo, 'ctrl_key:', g.ctrl_key, 'alt_key:', g.alt_key, 'shift_key:', g.shift_key))
    if g.debug_reset_app: print('    -', end='')
    g.tb.print_buffer()
    if g.debug_reset_app: print('    -cmdq: ', 'empty' if g.cmdq.empty() else '', '   unfinished tasks: ', g.cmdq.unfinished_tasks)
    for i in list(g.cmdq.queue):
        if g.debug_reset_app: print('       ', i, end='')

    if g.debug_reset_app:print('    -Call stack')
    if g.debug_reset_app:print_call_stack()

    g.mouse_key_combo = False
    g.ctrl_key = False
    g.alt_key = False
    g.shift_key = False
    g.kb.reset()
    g.tb.reset()
    g.click_state = False
    g.line_template_list = ''
    g.zone_template_list = ''
    g.symbol_template_list = ''
    g.ellipse_template_list = ''
    g.rect_template_list = ''

    while g.cmdq.unfinished_tasks > 0:
        g.cmdq.task_done()

# Todo: make sure an elent is selected before looking for a tmeplate or button.

def format_line_style(style):
    attempts = 0
    while attempts < 5:

        try:
            bar = get_format_bar()
            if g.debug_line: print('    -format bar found')
            bar_buttons = bar.find_elements_by_class_name('js-widget')
            if g.debug_line: print('    -bar buttons found')
            style_button = bar_buttons[3]
            style_button.click()
            if g.debug_line: print('    -style button clicked')
            wait = WebDriverWait(g.d, 5)
            if style == 'dotted':
                button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, cfg.line_dash_class)))
                if g.debug_line: print('    -dotted button found')
            elif style == 'solid':
                button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, cfg.line_solid_class)))
                if g.debug_line: print('    -solid button found')
            else:
                if g.debug_line: print('invalid line style in format_line_style()')
            time.sleep(1)
            button.click()
            break

        except (SX.StaleElementReferenceException, SX.NoSuchElementException):
            pass

        attempts += 1



def set_vis_level(vis_level):
    attempts = 0
    max_attempts = 5

    while attempts < max_attempts:
        try:
            bar = get_format_bar()
            if g.debug_line: print('    -format bar found')
            bar_buttons = bar.find_elements_by_class_name('js-widget')
            if g.debug_line: print('    -bar buttons found')
            setting_button = bar_buttons[6]
            setting_button.click()
            if g.debug_line: print('    -settings button clicked')
            wait = WebDriverWait(g.d, 5)
            settings_dialog = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, cfg.settings_dialog_css)))
            if g.debug_line: print('    -settings dialog found')
            visibility_tab = settings_dialog.find_element('Visibility')
            visibility_tab.click()

        except:
            pass

    pass


def key_capture_paused():
    print("capture paused")
    g.mouse_key_combo = False
    if cfg.debug_flow:
        print('return true because of pause in onkeyboard')
        print('-----')


# # reset expired alerts
# def restart_alerts():
#     ready = False
#     while not ready:
#         try:
#             is_active = "active" in g.alerts_button().get_attribute("class")
#             if not is_active:
#                 g.alerts_button().click()
#         except SX.StaleElementReferenceException:
#             ready = False
#             print('stale alerts button ... fixing ')
#         except SX.TimeoutException:
#             print('alerts button timeout')
#
#     alerts_table = g.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, config.alerts_table_css)))
#     restart_button_list = alerts_table.find_elements_by_class_name(config.alert_row_restart_class)
#     for restart_button in restart_button_list:
#         g.builder.move_to_element(restart_button).perform()
#         g.wait.until(EC.element_to_be_clickable, restart_button)
#         restart_button.click()



# class to manage key blocking.  must block both up and down or block neither
# global object definitions so the IDE can provide inspection
class KeyDownBuffer():
    '''

    '''

    def __init__(self):
        self.__buffer = []

    def reset(self):
        self.__buffer = []

    def key_in_buffer(self, key):
        if key in self.__buffer:
            return True
        else:
            return False

    def add_key(self, key):
        if not self.key_in_buffer(key):
            self.debug()
            return False
        else:
            self.__buffer.append(key)
            self.debug()

    def remove_key(self, key):
        if self.key_in_buffer(key):
            self.__buffer.remove(key)
            self.debug()
            return True
        else:
            return False

    def debug(self):
        if cfg.debug_key_buffer:
            self.print_buffer()

    def print_buffer(self):
        print('Key Buffer:', end=' ')
        for b in self.__buffer:
            print(b, end=' ')
        print('')


class ToolBuffer():
    def __init__(self):
        self.key = ''
        self.mod1 = ''
        self.mod2 = ''
        self.interval = ''
        self.same_tool = False
        self.same_interval = False

    def is_same(self, tool='', interval=''):
        return self.same_interval and self.same_tool

    def add_tool(self, key, mod1, mod2=''):
        if (key == self.key) and (mod1 == self.mod1) and mod2 == self.mod2:
            self.key = key
            self.mod1 = mod1
            self.mod2 = mod2
            self.same_tool = True
        else:
            self.same_tool = False
            self.key = key
            self.mod1 = mod1
            self.mod2 = mod2

    def add_interval(self, interval):
        if interval == self.interval:
            self.interval = interval
            self.same_interval = True
        else:
            self.same_interval = False
            self.interval = interval

    def reset(self):
        self.key = ''
        self.mod1 = ''
        self.mod2 = ''
        self.interval = ''

    def print_buffer(self):
        print('Tool Buffer: ', 'Tool: ', self.key, '   Mod1: ', self.mod1, '   Mod2: ', self.mod2,  '   Interval: ', self.interval, '    Same tools_bar_keymap: ', self.same_tool,
              '   Same interval: ', self.same_interval)
