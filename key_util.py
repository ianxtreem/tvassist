import PyHook3
from PyHook3 import HookConstants as Keys
import pythoncom
import win32api
import win32con

def onclick(event):

    print('MessageName:', event.MessageName)
    print('Message:', event.Message)
    print('Time:', event.Time)
    print('Window:', event.Window)
    print('WindowName:', event.WindowName)
    print('Position:', event.Position)
    print('Wheel:', event.Wheel)
    print('Injected:', event.Injected)
    print('---')

    return True

def OnKeyUp(event):

    print('MessageName:', event.MessageName)
    print('Message:', event.Message)
    print('Time:', event.Time)
    print('Window:', event.Window)
    print('WindowName:', event.WindowName)
    print('Asacii:', event.Ascii, chr(event.Ascii))
    print('Key:', event.Key)
    print('KeyID:', event.KeyID)
    print('ScanCode:', event.ScanCode)
    print('Extended:', event.Extended)
    print('Injected:', event.Injected)
    print('Alt', event.Alt)
    print('Transition', event.Transition)
    print('---')

    return False


def print_win32_keystate():
    print('win32\t', str(win32api.GetKeyState(win32con.VK_LCONTROL)),  '\t', str(win32api.GetKeyState(win32con.VK_RCONTROL)), '\t', str(win32api.GetKeyState(win32con.VK_LMENU)), '\t', str(win32api.GetKeyState(win32con.VK_RMENU)))


def print_win32_akeystate():
    print('win32a\t', str(win32api.GetAsyncKeyState(win32con.VK_LCONTROL)),  '\t', str(win32api.GetAsyncKeyState(win32con.VK_RCONTROL)), '\t', str(win32api.GetAsyncKeyState(win32con.VK_LMENU)), '\t', str(win32api.GetAsyncKeyState(win32con.VK_RMENU)))


def print_py3_keystate():
    print('py3:\t', PyHook3.GetKeyState(162), '\t', PyHook3.GetKeyState(163), '\t', PyHook3.GetKeyState(164), '\t', PyHook3.GetKeyState(165))


def just_key(event):



    print('MessageName:', event.MessageName)
    print('Key:', event.Key)
    print_win32_keystate()
    #print_win32_akeystate()
    #print_py3_keystate()
    print('Hit X to exit\n')

    if (event.Key == 'X'):
        exit(0)
        return True
    else:
        return False


def OnKeyDown(event):


    print('MessageName:', event.MessageName)
    print('Message:', event.Message)
    print('Time:', event.Time)
    print('Window:', event.Window)
    print('WindowName:', event.WindowName)
    print('Ascii:', event.Ascii, chr(event.Ascii))
    print('Key:', event.Key)
    print('KeyID:', event.KeyID)
    print('ScanCode:', event.ScanCode)
    print('Extended:', event.Extended)
    print('Injected:', event.Injected)
    print('Alt', event.Alt)
    print('Transition', event.Transition)
    print('---')

    if (event.Key == 'X'):
        exit(0)
        return True
    else:
        return False


while True:

    try:
        hm = PyHook3.HookManager()
    except:
        print('no hook manager')

    hm.KeyAll = just_key
    # hm.KeyAll = OnKeyDown
    #hm.KeyUp = OnKeyUp
    hm.HookKeyboard()
    #hm.MouseLeftDown = onclick
    #hm.HookMouse()
    pythoncom.PumpMessages()
    # hm.UnhookMouse()
