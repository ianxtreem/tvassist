# TVAssist

## Captures keystrokes and mouse clicks to streamline interaction with the TradingView website.  It performs the following functions:

***Please read every part of this documentation.  There is a lot to know to get it working
for you.***

### Usage:
```python
python TVAssist.py
```

### Installation and dependancies:

   Install [Python 3.6][1]
   
   Install [Chrome Driver][2]. The path to this must be in your PATH environmental variable
   
   Install the PyHook3, pythoncom, and selenium modules from a terminal window that 
   has your python installation in your path
   ```python
pip install pyhook3==1.6.1
pip install pywin32
pip install selenium
```

   Download the code from my [repository][3]
   
   Create a file named creds.py in the directory you download TVAssist  to.  Edit the file and add your username and password. 
```python
username='your username'
password='your password'  
```

### Known issues:
    - There are some problems applying the templats for the various text tools.  Will fix when I have time.
    - TV Assist expects the user to not do stupid things like try to apply a tool or template that doesnt exist.
        I am working on this as I do stupid things.  If you do something stupd and TVAssist breaks let me know

### Notes:

    - THIS CODE IS TO BE USED ON AN AS IS BASIS.  ALTHOUGH I CANT IMAGINE A CASE WHERE THIS IS APPLICABLE, LET IT BE
       KNOWN THAT I TAKE NO RESPONSIBILITY FOR WHAT IT MAY DO TO YOUR MACHINE OR YOUR SOFTWARE. IT HAS BEEN TESTED ON
       EXACTLY 1 MACHINE running Windows 10.
    - This code works in conjunction with pre-defined trading view drawing tool templates. 
        You will have to create those yourself.  I dont know how to export them.  I have included some screen shots
        at the bottom to help you.
    - TV Assist opens an instance of chromium and the tradingview website
    - Logs into tradingview and clicks on the chart button.  (this take you to your default chart, the one at the top
         of your list)
    - Listens for the keystrokes defined in the config.py file, selects the appropriate tool, and formats the object
    - In general CTRL key combinations select a tool and ALT key combinations provide a format for a selected tool.
        The CTRL key combinations are initiated when the user clicks somewhere on the chart after pressing the keys.
    - I have set mine up so that supply and demand zone are shown at current and below time frames, but you can set
        yours up however you like.
    - There is still a lot of optimization and testing to do
    - If you modify the code and make it better, i sure would appreciate a copy.
 
    
### Suggested TV templates:

  TVAssist is completely configurable via the config file. In the config file you provide the information necessary to
  key strokes to TV template names and TV favorites bar tools. Below is my configuration, yours can be 
  whatever you want.  You only need templates for things you want to have unique formatting.
  
    Supply and Demand: sf3Month, sfMonth, sfWeek, sfDay, sf4h
    Lines showing at current time frame only: sf3MonthRAY, sfMonthRAY, sfWeekRAY, sfDayRAY, sf4hRAY, sf3Month, sfMonth, sfWeek, sfDay, sf4h
    Lines show at current and below time frames: sf3MonthAB, sfMonthAB, sfWeekAB, sfDayAB, sf4hAB
    RR measure: sf3MonthRR, sfMonthRR, sfWeekRR, sfDayRR, sf4hRR
    Broken: sf3MonthBR, sfMonthBR, sfWeekBR, sfDayBR, sf4hBR
    Potenal Zone: sfMonthPO, sfWeekPO, sfDayPO, sf4hPO
    Used Up: sfMonthUD, sfWeekUD, sfDayUD, sf4hUD
    Text: sf3Month, SfMonth, sfWeek, sfDay, sf4h
    Anchored Text: SfMonthAT, sfWeekAT, sfDayAT, sf4hAT
    Callouts: sf3MonthCO, SfMonthCO, sfWeekCO, sfDayCO, sf4hCO
    Arrows: sfMonthUP, sfMonthUPC, sfMonthDN, sfMonthDNC
    Ellipse: sf3Month, SfMonth, sfWeek, sfDay, sf4h
    50% Fib: SF3Month50, sfMonth50, SFWeek50
    
### Suggested Keys:
#### Note: These keys and their functions are completely arbitrary and can be changed by the user in the cfg.py file

###### Zone tools
    CTRL-s     Creates a supply/demand zone at current timeframe
    Alt-p      Formats zone as potential zone
    Alt-b      Formats for a broken style
    Alt-u      Formats for a used style
    Alt-c      Formats a line for the current time frame
    Alt-f      Formats zone to show only the 50% line
    CTRL-w     Creates a risk reward  in current time frame
    
###### Trend line tools
    CTRL-l     Creates a ray at the current time frame
    CTRL-i     Creates a line in current time frame
    Alt-.      Changes line style to dotted
    Alt--      Changes the selected line style to solid
###### Elipse tools
    CTRL-e     Creates an ellipse in current time frame
###### Text tools
    CTRL-      Selects and formats a label 'un-anchored text.
    CTRL-k     Selects and formats an anchored text tool.
    CTRL-o     Selects and formats a callout.
###### Arrow tools    
    CTRL-Down  Adds a DOWN formatted arrow
    CTRL-Left  Adds a DOWN consolidating formatted arrow
    CTRL-Up    Adds an UP consolidating formatted arrow
    CTRL-Right Adds an UP formatted arrow
    Alt-Down   Formats a selected symbol as a DOWN formatted arrow
    Alt-UP     Formats a selected symbol as a UP formatted arrow
    Alt-Left   Formats a selected symbol as a DOWN consolidating formatted arrow
    Alt-Right  Formats a selected symbol as a UP consolidating formatted arrow
###### Chart controls  
    Alt-a      Toggles the auto-scale button for the selected chart.
    CTRL-m     Toggles magnet mode
###### Application control    
    CTRL-r     Re-Initialize. If everything has gone bonkers, try this. 
    CTRL-]     Re-reads the config file (not tested)
    CTRL-End   Hard exit
    CTRL-r     Re-initializes static chart elements
    Pause      Pauses application

###### TV Native tools
    Alt-H and Alt-V are implemented in the trading view application for horizontal and vertical lines
      - may do something with formatting them later.
      
### Key Utility

        TVAssist works for my keyboard which is an old IBM Model M keyboard. I don't know if the events that are sent out 
        from your keyboard will be the same as mine.  You might have to modify the config.py to provide the event.Key
        name for your keyboard.  I have included a utility to help you get the keys that correspond to your keyboard.
        Run the included included key_util file and see what the key name is created when you press a key.  Then modify 
        the config.py to reflect that Key.

Usage:
```python
python key_util.py
```
        
    
### Example video and image

[![](http://img.youtube.com/vi/xJxkrmZ1xRs/0.jpg)](http://www.youtube.com/xJxkrmZ1xRs)
![](https://www.tradingview.com/x/hGJPP8cP/)         


## Template configuration examples:

#### RR tool config: (no way to add text to the lines so just remember that 3 - 2RR, 5 - 3RR, 7 - 4RR)
![RR Tool Config](./rr_tool_config.png)

#### My tool bar layout (if yours has a different order you have to modify the values of the tool class in the config.py file)
![Toolbar Order](toolbar_layout.png)

#### A daily zone configuration. The same as the weekly above with different colors and visibility time frames.
![Daily Zone](daily_zone_config.png)

#### The templates you need to have for zones, lines and ellipse:
![Zone Template](zone_templates.png)![Line Template](line_templates.png)![Ellipse Template](ellipse_day_config.png)

#### Broken and Used Templates:
![Broken Template](broken_template_config.png)![Used Template](weekly_used_template_config.png)

#### Zone Color Configuration:
![Zone Color Config](supply_color_config.png)

[1]:(https://www.python.org/downloads/)
[2]:(https://www.python.org/downloads/)
[3]:(https://gitlab.com/ianxtreem/tvassist)