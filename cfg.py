# Todo: automatically get tool indexes

auto_format = False

# element identifiers
get_url = 'https://www.tradingview.com'
body_css = 'body'
signin_css = 'body > div.tv-main > div.tv-header > div.tv-header__inner.tv-layout-width > ' \
             'div.tv-header__area.tv-header__area--right.tv-header__area--desktop > span.tv-header__dropdown-text > a'
signin_form_id = 'signin-form'
signin_form_last = 'body > div.tv-dialog__modal-wrap > span'
username_css = '#signin-form > div.tv-control-error > div > input'
password_css = '#signin-form > div.tv-signin-dialog__forget-wrap > div.tv-control-error > div > input'

login_button_css = '#signin-form > div.tv-signin-dialog__footer > div:nth-child(2) > button'
login_button_xp = '//*[@id="signin-form"]/div[3]/div[2]/button'
#login_button_css = '#signin-form > div.tv-signin-dialog__footer > div:nth-child(2) > button > span.tv-button__loader'

cookie_button_class = 'cookie-button'
cookie_button_text = 'Got it'
cookie_button_css = 'body > div.cookie-message > div.cookie-button-wrapper > span.cookie-button.cookie-button--mobile'

charts_button_css = 'body > div.tv-main > div.tv-header > div.tv-mainmenu > ul > li.tv-mainmenu__item.tv-mainmenu__item--chart > a'
#charts_button_class = 'tv-mainmenu__item--chart'
charts_button_class = 'tv-mainmenu__item--chart'
charts_button_xp = '/html/body/div[3]/div[2]/div[3]/ul/li[5]/a'

drawing_tools_class = 'tv-floating-toolbar__widget'
drawing_tools_css = 'body > div:nth-child(16) > div > div.tv-floating-toolbar__content.js-content.ui-sortable'
drawing_tools_xp = '/html/body/div[4]'

symbol_xp = '/html/body/div[1]/div[7]/div/div[1]/div[1]/div/div/div[2]/div/div/div[1]'
#symbol_xp = '/html/body/div[1]/div[2]/div/div[1]/div[1]/div/div/div[2]/div[1]'
symbol_class ='dropdown-3_ASLzSj-'
#magnet_xp = '/html/body/div[1]/div[2]/div/div[1]/div[1]/div/div/div[2]/div[6]'
magnet_xp = '/html/body/div[1]/div[7]/div/div[1]/div[1]/div/div/div[4]/div[1]/div/span'
#magnet_css = 'body > div.js-rootresizer__contents > div.layout__area--left > div > div.wrap-1h7U5nKd- > div.scrollWrap-3gtPS0Fe-.noScrollBar-ieMwbfur- > div > div > div:nth-child(4) > div:nth-child(1)'
magnet_css = 'body > div.js-rootresizer__contents > div.layout__area--left > div > div.wrap-1h7U5nKd- > div.scrollWrap-3gtPS0Fe-.noScrollBar-ieMwbfur- > div > div > div:nth-child(4) > div.button-263WXsg--.apply-common-tooltip.common-tooltip-vertical.isActive-2mI1-NUL- > div > span'
magnet_class = 'control-1TyEfSIx-'

#ruler_css = 'body > div.js-rootresizer__contents > div.layout__area--left > div > div.wrap-1h7U5nKd- > div.scrollWrap-3gtPS0Fe-.noScrollBar-ieMwbfur- > div > div > div:nth-child(3) > div:nth-child(1)'
ruler_css = 'body > div.js-rootresizer__contents > div.layout__area--left > div > div.wrap-1h7U5nKd- > div.scrollWrap-3gtPS0Fe-.noScrollBar-ieMwbfur- > div > div > div:nth-child(3) > div:nth-child(1) > div'
ruler_xp = '/html/body/div[1]/div[7]/div/div[1]/div[1]/div/div/div[3]/div[1]/div'

# interval_css = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div > div > div > div > div:nth-child(2)'
interval_css = 'body > div.js-rootresizer__contents > div.layout__area--top > div > div > div:nth-child(1) > div.wrap-5DN0XnS4- > div > div > div > div:nth-child(2)'
interval_xp = '//*[@id="header-toolbar-intervals"]'
interval_bar_id = 'header-toolbar-intervals'
annoying_pu_css = 'body > div._tv-dialog._tv-dialog-nonmodal.ui-draggable > div._tv-dialog-content.symbol-search-dialog > div.symbol-block > div.symbol-block-inputspacer > input'

format_bar_xp = '/html/body/div[17]/div'
format_bar_css = 'body > div.tv-floating-toolbar.tv-linetool-properties-toolbar.tv-grouped-floating-toolbar.ui-draggable > div > div.tv-floating-toolbar__drag.js-drag.ui-draggable-handle'
format_bar_css = 'body > div.tv-floating-toolbar.tv-linetool-properties-toolbar.tv-grouped-floating-toolbar.ui-draggable'
template_button_css = 'body > div.tv-floating-toolbar.tv-linetool-properties-toolbar.tv-grouped-floating-toolbar.ui-draggable > div > div.tv-floating-toolbar__content.js-content > div:nth-child(1)'
template_button_xp = '/html/body/div[16]/div/div[2]/div[1]/a'
template_button_class = 'a.tv-linetool-properties-toolbar__button.apply-common-tooltip'
template_button_dropped_css = 'body > div.tv-floating-toolbar.tv-linetool-properties-toolbar.tv-grouped-floating-toolbar.ui-draggable > div > div.tv-floating-toolbar__content.js-content > div:nth-child(1) > a'
template_ray_popup_css = 'body > div.charts-popup-list.popup-with-scroll'
template_zone_popup_css = 'body > div.charts-popup-list'
template_popup_css = 'body > div.charts-popup-list'

line_tools_class = 'tv-linetool-properties-toolbar__icon--line-props'
line_style_class = 'tv-linetool-properties-toolbar__icon--line-props'
line_dash_class = 'tv-grouped-floating-toolbar__sub-widget--slide-right-2'
line_solid_class = 'tv-grouped-floating-toolbar__sub-widget--slide-right-0'

save_button_xp = '/html/body/div[1]/div[5]/div/div/div[1]/div[2]/div/span[2]'
save_button_css = 'body > div.js-rootresizer__contents > div.layout__area--top.header-chart-panel > div > div > div.right > div.group.space-single.header-group-save-load > div > span.save.button.apply-common-tooltip.titled'

alerts_button_css = 'body > div.js-rootresizer__contents > div.layout__area--right > div > div.widgetbar-tabs > div > div.scrollWrap-3gtPS0Fe-.noScrollBar-ieMwbfur- > div > div > div:nth-child(2)'
#alerts_button_css = 'body > div.js-rootresizer__contents > div.layout__area--right > div > div.widgetbar-tabs > div > div.widgetbar-tabscontent > div:nth-child(3)'
#alerts_button_xp = '/html/body/div[1]/div[4]/div/div[2]/div/div[1]/div[3]'
alerts_button_xp = '/html/body/div[1]/div[3]/div/div[2]/div/div[1]/div/div/div[2]'
alerts_table_css = 'body > div.js-rootresizer__contents > div.layout__area--right > div > div.widgetbar-pages > div.widgetbar-pagescontent > div.widgetbar-page.active > div.widgetbar-widget.widgetbar-widget-alerts_manage > div.widgetbar-widgetbody.alerts-widget.alerts-manage-widget > div.widget-inner > table > tbody'
alert_row_restart_class = 'ctx-button--restart'
alerts_menu_css = 'body > div.js-rootresizer__contents > div.layout__area--right > div > div.widgetbar-pages > div.widgetbar-pagescontent > div.widgetbar-page.active > div.widgetbar-widget.widgetbar-widget-alerts_manage > div.widgetbar-widgetheader > div.widgetbar-headerspace > a.button.apply-common-tooltip.active.open'
alerts_restart_all = 'body > div.charts-popup-list.ch-popup.ch-settings-popup > a:nth-child(6) > span.title-expanded'

last_login_xp = '//*[@id="signin-form"]'
last_home_xp = '/html/body/div[3]/div[2]/div[3]'
last_home_xp = '//*[@id="fb-root"]'
last_home_xp = '//*[@id="userdata_el"]'
last_home_css = '#js-category-content > div > div > div.tv-feed.js-balancer'
last_element_xp = '/html/body/div[4]/div/div[2]/div[11]'
annoying_overlay_xp = '/html/body/div[17]/div/div/div/div[3]/div[1]'
not_now_css = 'body > div.tv-dialog__modal-wrap > div > div > div > div.tv-dialog__section.tv-dialog__section--actions.tv-dialog__section--actions-adaptive.tv-dialog__section--no-border > div.js-dialog__action-click.js-dialog__no-drag.tv-button.tv-button--default'
annoying_overlay_css = 'body > div.tv-dialog__modal-wrap > div > div > div > div.tv-dialog__section.tv-dialog__section--actions.tv-dialog__section--actions-adaptive.tv-dialog__section--no-border > div.js-dialog__action-click.js-dialog__no-drag.tv-button.tv-button--default'
auto_scale_css = 'body > div.js-rootresizer__contents > div.layout__area--center > div.chart-container.multiple.active > div.chart-controls-bar > div.chart-controls-bar-buttons.chart-series-controls > a:nth-child(5)'
chart_series_control_bar_css = 'body > div.js-rootresizer__contents > div.layout__area--center > div.chart-container.active.multiple > div.chart-controls-bar > div.chart-controls-bar-buttons.chart-series-controls'

settings_dialog_css = 'body > div._tv-dialog._tv-dialog-nonmodal.ui-draggable'
visibility_tab_css = 'body > div._tv-dialog._tv-dialog-nonmodal.ui-draggable > div._tv-dialog-content > div.properties-tabs.tv-tabs.ui-draggable-handle > a.properties-tabs-label.tv-tabs__tab.active'



# keys to always pass to the underlying application
always_pass = ['Space', 'Tab', 'Rshift', 'Lshift', 'Back', 'Capital', 'Delete']
#always_pass = ['Space', 'Tab', 'Rshift', 'Lshift', 'Back', 'Capital', 'Delete', 'Lcontrol', 'Rcontrol', 'Escape']
alt_key_pass = ['G', 'H', 'V', 'W', 'Space', 'Z']
ctrl_key_pass = ['Space', 'C', 'V']
shift_key_pass = ['Space']

# increase this delay if you get errors about an element not being found
extra_delay = .5

# name of keys sent in the keyboard event and the corresponding format/ action name.
# Use key_util.py to inspect key messages and add your own.

ctrl_symbol_keys = {
    'Up': 'arrowup',
    'Down': 'arrowdn',
    'Right': 'arrowupcon',
    'Left': 'arrowdncon'
}

ctrl_line_keys = {
    'L': 'ray',
    'I': 'line'
}

ctrl_zone_keys = {
    'S': 'zone',
    'N': 'cont',
    'P': 'potential',
    'W': 'RR',
    'U': 'used',
    'B': 'broken'
}

ctrl_text_keys = {
    'T': 'atext',
    'U': 'utext',
    'O': 'callout'
}

ctrl_cmd_keys = {
    'M': 'magnet',
    'R': 'ruler',
    'Oem_1': 'config',
    'End': 'exit',
    'Pause': ''
}

ctrl_ellipse_keys = {
    'E': 'ellipse'
}

ctrl_rect_keys = {
    'Q': 'rect'
}

ctrl_keys = {**ctrl_zone_keys,
             **ctrl_line_keys,
             **ctrl_text_keys,
             **ctrl_symbol_keys,
             **ctrl_cmd_keys,
             **ctrl_ellipse_keys,
             **ctrl_rect_keys
}


tools_bar_keymap = {
    'L': 0,
    'I': 1,
    'S': 3,
    'N': 3,
    'P': 3,
    'W': 3,
    'U': 3,
    'B': 3,
    'E': 4,
    'T': 9,
    'Q': 10
}



alt_cmd_keys = {
    'A': 'auto',
    'M': 'magnet',
    'R': 'ruler'
}

alt_line_keys = {
    'L': 'ray',
    'I': 'line'
}

alt_zone_keys = {
    'S': 'zone',
    'B': 'broken',
    'U': 'used',
    'P': 'potential',
    'N': 'cont',
    'F': 'fifty',
    'W': 'RR'
}

alt_symbol_keys = {
    'Up': 'arrowup',
    'Down': 'arrowdn',
    'Right': 'arrowupcon',
    'Left': 'arrowdncon'
}

alt_line_style_keys = {
    'Oem_Period': 'dotted',
    'Oem_Minus': 'solid',
}

alt_vis_keys = {
    '1': 'one_level',
    '2': 'two_level'
}

alt_text_template_keys = {}
alt_rect_template_keys = {}
alt_ellipse_template_keys = {}

alt_keys = {**alt_cmd_keys,
            **alt_line_keys,
            **alt_zone_keys,
            **alt_symbol_keys,
            **alt_text_template_keys,
            **alt_line_style_keys}


# maps the current selected interval to a TV template prefix.  Change this as you like.
interval_to_prefix = {
    '4h': 'sf4h',
    '1D': 'sfDay',
    '1W': 'sfWeek',
    '1M': 'sfMonth',
    '3M': 'sf3Month',
}

# maps the format names listed in ctrl_keys, alt_template_mods, alt_line_style_keys, alt_symbol_keys to TV template
# name postfixes.  You can modify, but you should have a TV template for each time in this map.

format_to_postfix = {
    'sfMonth': {
        'ray': 'RAY',
        'line': '',
        'cont': 'CP',
        'zone': '',
        'atext': 'AT',
        'utext': 'TX',
        'callout': 'CO',
        'ellipse': '',
        'arrowup': 'UP',
        'arrowdn': 'DN',
        'arrowupcon': 'UPC',
        'arrowdncon': 'DNC',
        'broken': 'BR',
        'used': 'UD',
        'current': '',
        'below': 'AB',
        'potential': 'PO',
        'rect': '',
        'fifty': '50'
    },
    'sfWeek': {
        'ray': 'RAY',
        'line': '',
        'cont': 'CP',
        'zone': '',
        'atext': 'AT',
        'utext': 'TX',
        'callout': 'CO',
        'ellipse': '',
        'arrowup': 'UP',
        'arrowdn': 'DN',
        'arrowupcon': 'UPC',
        'arrowdncon': 'DNC',
        'broken': 'BR',
        'used': 'UD',
        'current': '',
        'below': 'AB',
        'potential': 'PO',
        'rect': '',
        'fifty': '50'
    },
    'sfDay': {
        'ray': 'RAY',
        'line': '',
        'cont': 'CP',
        'zone': '',
        'atext': 'AT',
        'utext': 'TX',
        'callout': 'CO',
        'RR': 'RR',
        'ellipse': '',
        'arrowup': 'UP',
        'arrowdn': 'DN',
        'arrowupcon': 'UPC',
        'arrowdncon': 'DNC',
        'broken': 'BR',
        'used': 'UD',
        'current': '',
        'below': 'AB',
        'potential': 'PO',
        'rect': '',
        'fifty': '50'
    },
    'sf4h': {
        'ray': 'RAY',
        'line': '',
        'cont': 'CP',
        'zone': '',
        'atext': 'AT',
        'utext': 'TX',
        'callout': 'CO',
        'RR': 'RR',
        'ellipse': '',
        'arrowup': 'UP',
        'arrowdn': 'DN',
        'arrowupcon': 'UPC',
        'arrowdncon': 'DNC',
        'broken': 'BR',
        'used': 'UD',
        'current': '',
        'below': 'AB',
        'rect': '',
        'potential': 'PO'
    },
    'sf3Month': {
        'ray': 'RAY',
        'line': '',
        'zone': '',
        'rect': '',
        'potential': 'PO',
        'atext': 'AT',
        'utext': 'TX',
        'callout': 'CO',
        'ellipse': '',
        'current': '',
        'below': 'AB',
        'fifty': '50',
        'arrowup': 'UP',
        'arrowdn': 'DN',
        'arrowupcon': 'UPC',
        'arrowdncon': 'DNC'
    }
}


