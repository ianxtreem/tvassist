import queue
import threading

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait



import TVAssistLib

chart_name = ''
ctrl_key = False
alt_key = False
shift_key = False
pause_key = False
click_state = False
cmdq = None  # type: queue.Queue
cmd_proc_thread = None  # type: threading.Thread

d = ''  # type: webdriver
window_handle = ''  # type str
builder = ''  # type: ActionChains
body_element = ''  # type: WebElement
drawing_tools = ''  # type: WebElement
symbol_button = ''  # type: WebElement
alerts_button = ''  # type: WebElement
magnet_button = ''  # type: WebElement
ruler_button = ''  # type: WebElement
interval_bar = ''  # type: WebElement
save_button = ''  # type: () -> WebElement
wait = ''  # type: WebDriverWait
long_wait = ''  # type: WebDriverWait
tb = ''  # type: TVAssistLib.ToolBuffer
kb = ''  # type: TVAssistLib.KeyDownBuffer
tt_format = ''  # type: str
mouse_key_combo = False
two_click_cmd = False
pause = False
last_error = ''  # type: str
ttw = 20  # time to wait for an element
zone_template_list = []
text_template_list = []
line_template_list = []
ellipse_template_list = []
rect_template_list = []
symbol_template_list = []

session_id = ''

debug_zone = False
debug_line = False
debug_alt = False
debug_ctrl = False
debug_templates = False
# debug flags
debug1 = False
debug_click = False
debug_key = False
debug_state = False
debug_key_block = False
debug_flow = False
debug_key_buffer = False
debug_reset_app = False

class EmptyTemplateList():
    '''
    Excption thrown if the tmeplate list is not populated
    '''