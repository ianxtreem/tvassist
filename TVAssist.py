import importlib
import inspect
import os
import queue
from threading import Event, Thread

import PyHook3
import pythoncom

from PyHook3 import HookConstants as Py3Keys

import winsound
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys
import creds
from TVAssistLib import *

import cfg
import g

def print_click_message(event):
    print('MessageName:', event.MessageName)
    print('Message:', event.Message)
    print('Time:', event.Time)
    print('Window:', event.Window)
    print('WindowName:', event.WindowName)
    print('Position:', event.Position)
    print('Wheel:', event.Wheel)
    print('Injected:', event.Injected)
    print('---')


def print_location():
    print(inspect.stack()[1][3])


def onclick(event):


    # exit if this is not a part of a command
    if not g.mouse_key_combo:
        return True
    if g.click_state:
        return False
    if g.pause:
        return True
    if not cfg.auto_format:
        try: g.cmdq.task_done()
        except ValueError: pass
        g.click_state = False
        g.mouse_key_combo = False
        return True


    g.click_state = True
    if g.mouse_key_combo:
        if (event.MessageName == 'mouse left down'):
            if g.tt_format != '':
                apply_format(g.tt_format)
                g.cmdq.task_done()
                g.click_state = False
                g.mouse_key_combo = False
            return True

    # return true if this had nothing to do with mouse_key_combo action
    return True


# Todo: maximize/minimize pane
# Todo: search smaller sections of the web page

def process_alt_cmds(key):
    if cfg.alt_cmd_keys[key] == 'auto':
        g.wait = WebDriverWait(g.d, 3)
        elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.auto_scale_css)))
        elem.click()
    if cfg.alt_cmd_keys[key] == 'magnet':
        g.magnet_button.click()
        # g.wait = WebDriverWait(g.d, 3)
        # elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.magnet_css)))
        # elem.click()
    if cfg.alt_cmd_keys[key] == 'ruler':
        g.wait = WebDriverWait(g.d, 3)
        elem = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.ruler_css)))
        elem.click()

def process_alt_event(key, escape=False, delay=0):

    if g.pause:
        return

    if escape:
        ready = False
        while not ready:
            try:
                time.sleep(delay)
                g.body_element = get_element(cfg.body_css)
                g.body_element.send_keys(Keys.ESCAPE)
                ready = True
            except SX.StaleElementReferenceException:
                print('stale body element in process_alt_event()')
                reset_app_state()
    if key in cfg.alt_cmd_keys:
        g.tb.add_tool(key, "ALT")
        process_alt_cmds(key)
        g.tb.reset()
        g.cmdq.task_done()
    elif key in cfg.alt_line_style_keys:
        format_line_style(cfg.alt_line_style_keys[key])
        g.cmdq.task_done()
    elif key in cfg.alt_vis_keys:
        set_vis_level(cfg.alt_vis_keys[key])
        g.cmdq.task_done()
    elif key in cfg.alt_keys:     # all the rest of the alt keys
        g.tb.add_tool(key, "ALT")
        if cfg.auto_format:
            apply_format(cfg.alt_keys[key])
        g.tb.reset()
        try:
            g.cmdq.task_done()
        except ValueError:
            pass
    else:
        print('Unknown alt mod key')

@staleRefHandler
def process_ctrl_event(key):

    tool_selected = False
    if key in cfg.tools_bar_keymap:
        g.tb.add_tool(key, 'CTRL')
        g.drawing_tools[cfg.tools_bar_keymap[key]].click()
        g.tt_format = cfg.ctrl_keys[key]
        g.mouse_key_combo = True
    elif key in cfg.ctrl_symbol_keys:
        g.tb.add_tool(key, 'CTRL')
        g.symbol_button.click()
        g.tt_format = cfg.ctrl_symbol_keys[key]
        g.mouse_key_combo = True
    elif key in cfg.ctrl_cmd_keys:
        if cfg.ctrl_cmd_keys[key] == 'exit':
            g.tb.add_tool(key, 'CTRL')
            g.d.quit()
            os._exit(1)
        elif cfg.ctrl_cmd_keys[key] == 'magnet':
            g.tb.add_tool(key, 'CTRL')
            g.magnet_button.click()
            g.cmdq.task_done()
            g.tb.reset()
        elif cfg.ctrl_cmd_keys[key] == 'ruler':
            g.tb.add_tool(key, 'CTRL')
            g.magnet_button.click()
            g.cmdq.task_done()
            g.tb.reset()
        elif cfg.ctrl_cmd_keys[key] == 'config':
            g.tb.add_tool(key, 'CTRL')
            importlib.reload(cfg)
            g.cmdq.task_done()
            g.tb.reset()
        elif cfg.ctrl_cmd_keys[key] == 'init':
            g.tb.add_tool(key, 'CTRL')
            init_static_chart_elements()
            g.cmdq.task_done()
            g.tb.reset()
        else:
            pass
    else:
        pass

def process_ctrl_alt_event(event):
        # Todo: implement this as a reverse map lookup later
        if event.KeyID == ord('L'):
            g.line_template_list.clear()
        elif event.KeyID == ord('S'):
            g.zone_template_list.clear()
        elif event.KeyID == ord('W'):
            g.symbol_template_list.clear()
        elif event.KeyID == ord('E'):
            g.ellipse_template_list.clear()
        elif event.KeyID == ord('Q'):
            g.rect_template_list.clear()


# keep track of the state of modifier keys
def update_mod_state(event):

    if (event.KeyID == Py3Keys.vk_to_id['VK_LCONTROL']) or (event.KeyID == Py3Keys.vk_to_id['VK_RCONTROL']):
        if event.MessageName == 'key down':
            g.ctrl_key = True
        elif event.MessageName == 'key up':
            g.ctrl_key = False
        else:
            print('should not be here...  ctrl key update_mod_state')
    elif (event.KeyID == Py3Keys.vk_to_id['VK_LSHIFT']) or (event.KeyID == Py3Keys.vk_to_id['VK_RSHIFT']):
        if event.MessageName == 'key down':
            g.shift_key = True
        elif event.MessageName == 'key up':
            g.shift_key = False
        else:
            print('should not be here... shift key update_mod_state')
    elif event.KeyID == Py3Keys.vk_to_id['VK_PAUSE']:
        if event.MessageName == 'key down':
            if g.pause_key:
                g.pause_key = False
                g.pause = False
                print('\nKey Capture Active:')
            elif not g.pause_key:
                g.pause_key = True
                g.pause = True
                print('\nKey Capture Paused', end='')
        elif event.MessageName == 'key up':
            pass
        else:
            print('should not be here... pause key update_mod_state')

    g.alt_key = bool(event.IsAlt())


def OnKeyboardEvent(event):

    if not cmd_proc_thread.is_alive():
        print('bubba thread is dead. :(')
        exit(99)

    update_mod_state(event)
    event.ctrl = g.ctrl_key
    event.alt = g.alt_key
    event.shift = g.shift_key

    # if the escape key is hit, reset all buffers and clear the cmd queue
    if event.KeyID == Py3Keys.vk_to_id['VK_OEM_3']:
        if event.MessageName == 'key down':
            reset_app_state()
            print('    -window handles:', event.Window, g.window_handle)
            init_static_chart_elements()
            return False
        else:
            return False


    # Only capture if tradingview is the top window
    if event.Window == g.window_handle:
        pass
    elif event.Key == 'R':
        pass
    else:
        return True


    if g.pause:
        print('.', end='')
        return True

    if event.shift:
        return True
    elif not (event.ctrl or event.alt):
        return True
    elif event.Key in cfg.always_pass:
        return True
    elif event.ctrl and (event.Key in cfg.ctrl_key_pass):
        return True
    elif (event.Key not in cfg.ctrl_keys) and (event.Key not in cfg.alt_keys):
        return True
    else:
        if (event.MessageName == 'key up') or (event.MessageName == 'key sys up'):
            return False
        else:
            g.cmdq.put(event)
            return False


def process_commands():

    while True:

        if not cmd_proc_thread.is_alive():
            print('bubba thread is dead. :(')
            exit(99)

        event = g.cmdq.get(block=True)
        g.click_state = False
        print('{:<20}{:<10}{:<10}{:<10}{:<10}'.format(event.MessageName, event.Key,
                                                      'ctrl' if event.ctrl else 0,
                                                      'alt' if event.alt else 0,
                                                      'shift' if event.shift else 0))

        # process ctrl-alt command queue messages
        if event.ctrl and event.alt and (event.KeyID in cfg.ctrl_keys):
            process_ctrl_alt_event(event.Key)

        # process control command queue messages
        if event.ctrl and (event.Key in cfg.ctrl_keys):
            process_ctrl_event(event.Key)

        # process alt command queue messages
        if event.alt and (event.Key in cfg.alt_keys):
            process_alt_event(event.Key)



# ***************************************************************************************************************
if __name__ == '__main__':

    # todo add command line processing for chart name

    g.cmdq = queue.Queue(10)
    init_browser()
    g.d.implicitly_wait(10)
    # Todo: merge update mod keys with the key buffer
    g.kb = KeyDownBuffer()
    g.tb = ToolBuffer()

    # open trading view in chromium
    g.d.get(cfg.get_url)
    # todo: use the below to reconnect  https://stackoverflow.com/questions/18721404/connect-to-an-already-running-instance-of-chrome-using-selenium-in-python
    g.url = g.d.command_executor._url
    g.session_id = g.d.session_id

    # set session height and width.
    # tw = 3840
    # th = 2160
    # x = 0
    # y = 650
    # g.d.set_window_size(width=tw, height=th - y - 400)
    # g.d.set_window_position(x=x, y=y)

    #waitfor = get_element(config.last_home_css, wait_type=EC.presence_of_all_elements_located)
    signin_element = get_element(cfg.signin_css, wait_type=EC.element_to_be_clickable)
    signin_element.click()

    # waitfor = get_element(cfg.signin_form_last, By.CSS_SELECTOR, EC.presence_of_element_located)  # wait for the form to be loaded
    username_element = get_element(cfg.username_css, By.CSS_SELECTOR, EC.presence_of_element_located)
    password_element = get_element(cfg.password_css, By.CSS_SELECTOR, EC.presence_of_element_located)
    login_button_element = get_element(cfg.login_button_css, By.CSS_SELECTOR, EC.element_to_be_clickable)

    username_element.send_keys(creds.username)
    password_element.send_keys(creds.password)
    login_button_element.click()

    # cookie_button = get_element(cfg.cookie_button_css, By.CSS_SELECTOR, EC.element_to_be_clickable)
    # cookie_button.click()

    charts_button = get_charts_button()
    charts_button.click()
    time.sleep(2)

    print('{:<20}{:<10}{:<10}{:<10}{:<10}'.format('MsgName', 'Key', 'Ctrl', 'Alt', 'Shift'))

    cmd_proc_thread = Thread(target=process_commands, name='Bubba')
    cmd_proc_thread.start()

    # dispense with the anoyying too many drawings overlay if it exists
    try:
        wait = WebDriverWait(g.d, 10)
        not_now_button = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, cfg.not_now_css)))
        time.sleep(1)
        not_now_button.click()
    except:
        pass

    g.body_element = get_element(cfg.body_css, By.CSS_SELECTOR, EC.presence_of_element_located)

    init_static_chart_elements()


    winsound.Beep(2500, 500)
    # setup the hooking of keyboard and mouse events.

    g.window_handle = GetForegroundWindow()

    while True:

        try:
            hm = PyHook3.HookManager()
        except Exception as err:
            print(err)
            print('no hook manager')

        hm.KeyAll = OnKeyboardEvent
        hm.MouseLeftDown = onclick
        hm.HookKeyboard()
        hm.HookMouse()
        pythoncom.PumpMessages()

    exit(0)
